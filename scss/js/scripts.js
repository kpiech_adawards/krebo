$(document).ready(function () {
    $('.mdb-select').materialSelect();

    $('.main-header .select-wrapper').prepend('<span class="main-header__down-arrow"></span>');   
    
    $('.navbar-toggler').on('click', function () {
        $('.main-header__close-menu-area').css('display', 'block');
        $('.shop-header__close-area').css('display', 'block');
    });

    $('.main-header__close-menu-area').on('click', function () {
        $('.main-header__close-menu-area').css('display', 'none');
        $('.navbar-collapse').removeClass('show');
    });

    $('.shop-header__close-area').on('click', function () {
        $('.shop-header__close-area').css('display', 'none'); 
        $('.navbar-collapse').removeClass('show');
    });

    $(".owl-carousel").owlCarousel({
        loop:true,
        nav:true, 
        responsive:{
            0:{
                items:1,
            },
            575:{
                items:2,
            },
            767:{
                items:3,
            },
            1000:{
                items:4,
                
            },
            1800:{
                items:5,
            }
        }
    }); 
});

